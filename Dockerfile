FROM alpine:latest
MAINTAINER Jose R. Gonzalez

RUN apk update && apk upgrade && apk add ruby ; rm -rf /var/cache/apk/*

ADD ./ /opt/httpollrb
WORKDIR /opt/httpollrb

ENTRYPOINT ["./httpoll.rb"]