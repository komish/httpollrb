Summary: Measure response times to an HTTP endpoint over a set duration and interval.
Name: httpollrb
Version: 1.0
Release: 1
License: MIT
BuildArch: noarch
Group: Tools
Packager: Jose R. Gonzalez
Requires: ruby
URL: https://gitlab.com/komish/httpollrb
Source: httpollrb.tar.gz

%description
Measure response times to an HTTP endpoint over a set duration and interval.

%prep
%setup -q

%build

%install
install -m 0755 -d $RPM_BUILD_ROOT/usr/local/bin
install -m 0755 httpoll.rb $RPM_BUILD_ROOT/usr/local/bin/

%files
/usr/local/bin/httpoll.rb


%changelog
* Mon Apr 29 2019 Jose R. Gonzalez  1.0
  - Initial rpm build