# HTTPollrb

Basic ruby script to measure response times to an HTTP endpoint over a set duration and interval. Run as a standalone script, use with Docker, or install via RPM.

## Sample Usage

```sh
# httpoll.rb -t "https://example.com/" -d 20 -i 1
Polling "https://example.com/" every 1second(s) for 20 second(s).
(200 OK) in 5.9806ms --- Overall Average 5.9806ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9216ms --- Overall Average 4.9511ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.7566ms --- Overall Average 4.5529ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9976ms --- Overall Average 4.4141ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 4.4241ms --- Overall Average 4.4161ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9923ms --- Overall Average 4.3455ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9852ms --- Overall Average 4.294ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 5.2026ms --- Overall Average 4.4076ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9078ms --- Overall Average 4.352ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.5527ms --- Overall Average 4.2721ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.982ms --- Overall Average 4.2457ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 4.1607ms --- Overall Average 4.2386ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.8805ms --- Overall Average 4.2111ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 4.1278ms --- Overall Average 4.2051ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9649ms --- Overall Average 4.1891ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9412ms --- Overall Average 4.1736ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.6457ms --- Overall Average 4.1426ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.8541ms --- Overall Average 4.1266ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.956ms --- Overall Average 4.1176ms --- HTTP Response Summary / 200, 100.0%
(200 OK) in 3.9857ms --- Overall Average 4.111ms --- HTTP Response Summary / 200, 100.0%
```
## Help

```sh
# httpoll.rb --help
Measure response times to an HTTP address over a set duration and interval.

Usage: httpoll.rb [options]
    -t, --target=TARGET              Target URL to poll. Default: http://example.com
    -d, --duration=DURATION          Duration to poll in seconds. Default: 60
    -i, --interval=INTERVAL          Interval to poll in seconds. Default 5
    -h, --help                       Prints this help output.
```

## Building a Docker Image
Clone this repository and run the following to use the provided Dockerfile

```
cd httpollrb
docker build -t httpollrb ./
```

## Use with Docker
```
docker run -it --rm httpollrb:latest [--args ...]
```

## Generate a RPM file

Clone this repository and run the following.

```bash
# prepare tools
yum install rpmdevtools rpmbuild
rpmdev-setuptree

# Build RPM
cd httpollrb
mkdir httpollrb-1.0 && cp -a httpoll.rb httpollrb-1.0/
tar czvf ~/rpmbuild/SOURCES/httpollrb.tar.gz httpollrb-1.0
cp -a rpmspec/httpollrb.spec ~/rpmbuild/SPECS/
cd ~/rpmbuild
rpmbuild -ba SPECS/httpollrb.spec
```