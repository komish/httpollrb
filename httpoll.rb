#!/usr/bin/env ruby

require "net/http"
require "benchmark"
require "optparse"

def get_response_summary(responses)
  ##
  # average all response times.
  # params:
  # +responses+:: hash of httprespcode,response_time (in ms)
  return "-" if responses.length <= 0

  total = 0
  responses.values.each do |value|
    total += value
  end

  percents = {}
  responses.keys.each do |key|
    percents[key] = (responses[key] / total.to_f) * 100
  end

  return percents
end

def pretty(percents)
  ##
  # pretty prints stringified output of percents hash
  # params:
  # +percents+:: hash of httprespcode,percentage
  output = ""
  percents.keys.each do |key|
    output << " / #{key}, #{percents[key].round(2)}%"
  end
  return output
end

def average_response_times(times)
  ##
  # === test
  # average response times (in ms)
  # params:
  # +times+:: an array of response times (in ms)
  return times.inject(0) { |total, time| (total + time) } / times.length
end

# our default values
options = {
  target: "http://example.com",
  duration: 60,
  interval: 5
}

# modify defaults with user input.
OptionParser.new do |opts|
  opts.banner = "Usage: httpoll.rb [options]"
  opts.on("-tTARGET", "--target=TARGET", String,
          "Target URL to poll. Default: http://example.com")\
          { |target| options[:target] = target }
  opts.on("-dDURATION", "--duration=DURATION", Integer,
          "Duration to poll in seconds. Default: 60")\
          { |duration| options[:duration] = duration }
  opts.on("-iINTERVAL", "--interval=INTERVAL", Integer,
          "Interval to poll in seconds. Default 5")\
          { |interval| options[:interval] = interval }
  opts.on("-h", "--help", "Prints this help output.") do
    puts "Measure response times to an HTTP address "\
         "over a set duration and interval.\n\n"
    puts opts
    exit
  end
end.parse!

# initialize what we need to execute.
i = 0
uri = URI(options[:target])
performance = []
responses = {}
responses.default = 0

# run poll
puts "Polling \"#{uri}\" every #{options[:interval]}"\
     "second(s) for #{options[:duration]} second(s)."
while i < options[:duration]
  # strange scoping things going on here...
  resp = 0
  time = Benchmark.measure do
    resp = Net::HTTP.get_response(uri)
  end
  responses[resp.code] += 1
  performance << time.real
  time_in_ms = (time.real * 100).round(4)
  average_response_in_ms = (average_response_times(performance) * 100).round(4)
  puts "(#{resp.code} #{resp.message}) in #{time_in_ms}ms "\
       "--- Overall Average #{average_response_in_ms}ms --- "\
       "HTTP Response Summary#{pretty(get_response_summary(responses))}"
  sleep options[:interval]
  i += options[:interval]
end
